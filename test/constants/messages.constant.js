export const PORTFOLIO_USER = 'Komal Rane';
export const ABOUT_ME_PAGE_HEADER = 'ABOUT ME';
export const DEVELOPMENT_PLATFORM = 'Development Platforms';
export const DP = [ 'Node.js', 'JavaScript', 'npm', 'Java', 'Maven', 'MYSQL' ];
export const AUTOMATION_TOOLS = [ 'WebdriverIO', 'Cucumber', 'Selenium', 'ChaiJS', 'JUnit5' ];
export const AUTOMATION_PLATFORM = 'Test Automation Frameworks';
export const FRONTEND = [ 'React JS', 'HTML5', 'CSS3', 'styled-components', 'Bootstrap' ];
export const FRONTEND_HEADER = 'Frontend';
export const PROJECT_MANAGEMENT_TOOLS_HEADER = 'Project Management Tools';
export const PROJECT_MANAGEMENT_TOOLS = [ 'Jira', 'Quality Center' ];
export const CONTINUOUS_INTEGRATION_HEADER= 'Continuous Integration';
export const API_TESTING_HEADER= 'API Testing';
export const HOSTING_PLATFORM_HEADER= 'Hosting Platforms';
export const CONTINUOUS_INTEGRATION = [ 'GitHub Pages', 'GITLAB', 'Jenkins' ] ;

export const API_TESTING = [ 'Postman', 'Swagger' ] ;

export const HOSTING_PLATFORM = [ 'Netlify', 'Google Firebase' ] ;