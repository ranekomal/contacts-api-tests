API Test Automation - Mocha, SuperTest, Chai
=================

This is an API Automation Tests project 

Requirements
---------------

- node >= 10.15.x - [how to install Node](https://nodejs.org/en/download/)
- jdk >= 1.8

Getting Started
---------------

Install the dependencies:

```bash
npm install
```

Run tests 

```bash
npm run test
```
